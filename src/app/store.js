import { configureStore } from '@reduxjs/toolkit';
import authReducer from '../components/Auth';
import messageSlice from '../components/Message';

export default configureStore({
  reducer: {
    auth: authReducer,
    messages: messageSlice,
  },
});
