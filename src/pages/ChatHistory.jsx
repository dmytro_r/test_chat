import React from 'react';
import Message from '../widgets/Message'

export default function ChatHistory(props) {
    let {myId, messages, deleteMessage} = props

    let messagesMarkup = messages.map( message => Message(message, message.from === myId, deleteMessage ) )

    return <div>
        {messagesMarkup}
    </div>
}