import React from 'react';
import { Redirect } from "react-router-dom";
import { useSelector } from 'react-redux';
import { isAuthorized } from "../components/Auth";

export default function Main() {
    const authorized = useSelector(isAuthorized)
    return authorized ? <Redirect to="/chat" /> : <Redirect to="/sign-in" />
}