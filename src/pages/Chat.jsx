import React, {useEffect } from 'react';
import ChatMessageForm from './ChatMessageForm'
import Container from '@material-ui/core/Container';
import { useSelector, useDispatch } from 'react-redux';
import {
    deleteMessage,
    message,
    getLast10Messages
} from '../components/Message'
import { userId as userIdSelector} from '../components/Auth'
import AppHeader from './AppHeader'
import ChatHistory from "./ChatHistory";
import {init, send, close} from '../services/Messages'
import {MessageDTO} from "../models/MessageDTO";

export default function Chat() {
    let messages = useSelector(getLast10Messages);
    let userId = useSelector(userIdSelector);
    const dispatch = useDispatch()

    let delMsg = uid => dispatch(deleteMessage(uid))

    let onSubmitMessage = (msg) => {
        send( JSON.stringify({from:userId, text: msg}))
    }
    let onReceivedMessage = (msg) =>{
        let data = JSON.parse(msg);
        dispatch( message(MessageDTO(data.from, data.text)))
    }

    useEffect(() => {
        init(onReceivedMessage)
        return () => {
            close()
        }
    },[]);

    return (
        <Container component="main" maxWidth="sm">
            <AppHeader />
            <ChatHistory myId={userId} messages={messages} deleteMessage={delMsg}/>
            <ChatMessageForm onSubmitMessage={onSubmitMessage}/>
        </Container>
    )
}