import React from 'react';
import { useDispatch } from 'react-redux';
import { signOut } from '../components/Auth'
import {Redirect} from "react-router-dom";

export default function SignOut() {
    const dispatch = useDispatch()
    dispatch(signOut())
    return <Redirect to="/" />
}