import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { CircularProgress } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function SignInForm({inProgress, error, onSubmit}) {
    const classes = useStyles();
    let [login, setLogIn] = React.useState('username')
    let [password, setPassword] = React.useState('password')

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign in
                </Typography>
                <form className={classes.form} noValidate>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="Email Address"
                        name="email"
                        autoComplete="email"
                        autoFocus
                        value={login}
                        onChange={
                            (e)=>{
                                setLogIn(e.target.value)
                            }
                        }
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        value={password}
                        onChange={
                            (e)=>{
                                setPassword(e.target.value)
                            }
                        }
                    />

                    { inProgress
                    ?   <CircularProgress/>

                    :   <Button
                            fullWidth
                            variant="contained"
                            color="primary"
                            disabled={ !(login && password)}
                            className={classes.submit}
                            onClick={
                                (e) => {
                                    e.nativeEvent.preventDefault()
                                    onSubmit(login, password)
                                }
                            }
                        >
                            Sign In
                        </Button>
                    }

                </form>
            </div>
        </Container>
    );
}