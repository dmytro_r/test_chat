import React from 'react';
import Button from "@material-ui/core/Button";
import ButtonGroup from '@material-ui/core/ButtonGroup';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { NavLink } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    selected: {
        color:'white'
    },
}));

export default function AppHeader() {
    const classes = useStyles()

    return (
        <AppBar position="static">
            <Toolbar>
                <ButtonGroup variant="text" aria-label="text primary button group">
                    <NavLink activeClassName={classes.selected} to={'/chat'}>{<Button>Chat</Button>}</NavLink>
                    <NavLink activeClassName={classes.selected} to={'/history'}>{<Button>History</Button>}</NavLink>
                    <NavLink activeClassName={classes.selected} to={'/sign-out'}>{<Button>Log out</Button>}</NavLink>
                </ButtonGroup>
            </Toolbar>
        </AppBar>
    )
}