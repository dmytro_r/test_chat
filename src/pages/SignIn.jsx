import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import SignInForm from './SignInForm'
import { Redirect } from "react-router-dom";

import {
    isInProgress,
    isAuthorized,
    SighIn
} from '../components/Auth'

export default function SignIn() {
    const dispatch = useDispatch()

    let handleSubmit = (username, password)=>{
        dispatch(SighIn(username, password))
    }
    let authorized = useSelector(isAuthorized)
    let authInProgress = useSelector(isInProgress)
    return authorized ? <Redirect to="/chat" /> : <SignInForm inProgress={authInProgress} error={false} onSubmit={handleSubmit}/>
}