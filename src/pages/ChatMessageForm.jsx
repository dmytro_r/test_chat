import React from 'react';
import Button from '@material-ui/core/Button';
import { TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    marginTop10: {
        marginTop:10
    },
}));

export default function ChatMessageForm({onSubmitMessage}) {
    let [message, setMessage] = React.useState('')
    let classes = useStyles()
    return (
        <React.Fragment>
            <TextField
                className={classes.marginTop10}
                fullWidth
                placeholder="Enter message"
                multiline={true}
                rows="10"
                onChange={
                    (e)=>{
                        setMessage(e.target.value)
                    }
                }
                value={message}
            />
            <Button
                className={classes.marginTop10}
                fullWidth
                variant="contained"
                color="primary"
                disabled={!message}
                onClick={
                    (e) => {
                        e.nativeEvent.preventDefault()
                        onSubmitMessage(message)
                        setMessage('')
                    }
                }
            >
                Send
            </Button>
        </React.Fragment>
    )
}