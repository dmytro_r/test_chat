import React from 'react';
import Container from '@material-ui/core/Container';
import { useSelector, useDispatch } from 'react-redux';
import {
    getAllMessages,
    deleteMessage
} from '../components/Message'
import { userId as userIdSelector} from '../components/Auth'
import AppHeader from './AppHeader'
import ChatHistory from './ChatHistory'

export default function History() {
    let messages = useSelector(getAllMessages);
    let userId = useSelector(userIdSelector);
    const dispatch = useDispatch()

    let del = uid => dispatch(deleteMessage(uid))

    return (
        <Container component="main" maxWidth="sm">
            <AppHeader />
            <ChatHistory myId={userId} messages={messages} deleteMessage={del}/>
        </Container>
    )
}