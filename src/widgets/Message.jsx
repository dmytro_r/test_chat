import React from 'react';
import './Message.css'

export default function Message(message, isOutgoing, onDelete) {
    let {from, text, deleted} = message;
    let handleDelete = (uid) => () => { onDelete(uid) }

    return <div className={"message " + (isOutgoing ? 'outgoing' : '')}>
        {from} : {deleted ? '-deleted-' : text}
        {isOutgoing  ?
            !deleted ? <div key={message.uid} className={'delete'} onClick={  handleDelete(message.uid) } >del</div> : null
            : null} </div>
}