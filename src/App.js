import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Main from './pages/Main'
import Chat from './pages/Chat'
import SignIn from './pages/SignIn'
import History from './pages/History'
import SignOut from './pages/SignOut'
import ProtectedRoute from './components/ProtectedRoute'
import { useSelector } from 'react-redux';
import { isAuthorized } from "./components/Auth";

function App() {
  return (
      <Router>
          <Switch>
              <Route path="/" exact component={Main} />
              <Route path="/sign-in" exact component={SignIn} />
              <Route path="/sign-out" exact component={SignOut} />
              <Route path="/history" exact component={History} />
              <ProtectedRoute path="/chat" exact  authed={useSelector(isAuthorized)} component={Chat} />
          </Switch>
      </Router>
  )
}
export default App;
