let socket;

export function init(onMessage){

            socket = new WebSocket("ws://echo.websocket.org");
            socket.onopen = function() {
                console.log("Connected");
            };
            socket.onmessage = function(event) {
                console.log("New Data " + event.data);
                onMessage( event.data)
            };
            socket.onclose = function(event) {
                if (event.wasClean) {
                    console.log('Closed clean');
                } else {
                    console.log('Connection dropped');
                }
                console.log('Code: ' + event.code + ' Reason: ' + event.reason);
            };
            socket.onerror = function(error) {
                console.log("Error " + error.message);
            };
}

export function send(msg) {
    socket.send(msg);
}

export function close (){
    socket.close()
}