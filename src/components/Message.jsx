import { createSlice } from '@reduxjs/toolkit'
import { MessageDTO } from '../models/MessageDTO'

export const messageSlice = createSlice({
    name: 'messages',
    initialState: {
        messages: [
            MessageDTO(987, 'hi there'),
        ]
    },
    reducers: {
        message: (state, action) => {
            state.messages.push(action.payload)
        },
        deleteMessage:(state, action) => {
            let messageUid = action.payload
            for (let i = 0,j=state.messages.length; i < j; i++) {
               let message = state.messages[i]
                if(message.uid === messageUid) {
                    message.deleted = true
                    break
                }

            }
        },
    },
});

export const { message, deleteMessage } = messageSlice.actions;

export const sendEchoMessage = ( toId, message) => dispatch => {

}

export const getLast10Messages = state => state.messages.messages.slice(Math.max(state.messages.messages.length - 10, 0))
export const getAllMessages = state => state.messages.messages

export default messageSlice.reducer