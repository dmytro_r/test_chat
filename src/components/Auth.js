import { createSlice } from '@reduxjs/toolkit'
import { SignIn as SignInApiCall } from '../services/Auth'

export const authSlice = createSlice({
    name: 'auth',
    initialState: {
        tokenExpiry: localStorage.getItem('tokenExpiry') || new Date().getTime(),
        inProgress:  false,
        userId: localStorage.getItem('userId') || void 0
    },
    reducers: {
        signInRequest: (state, action) => {
            state.inProgress = true
        },
        signInSuccess: (state, action) => {
            let {userId, tokenExpiry} = action.payload
            state.tokenExpiry = tokenExpiry
            state.userId = userId

            state.inProgress = false
        },
        signOut: state => {
            state.tokenExpiry = 0
            state.inProgress= false
            state.userId = void 0
        },
    },
});

export const { signInRequest, signInSuccess, signOut } = authSlice.actions;

export const SighIn = ( username, password) => dispatch => {
    dispatch( signInRequest() )

    SignInApiCall( username, password ).then( ({userId, tokenExpiry} ) => {
            localStorage.setItem('tokenExpiry', tokenExpiry)
            localStorage.setItem('userId', userId)
            dispatch(signInSuccess({userId:userId, tokenExpiry: tokenExpiry}))
        })
}

export const isAuthorized = state => state.auth.tokenExpiry > new Date().getTime()
export const isInProgress = state => state.auth.inProgress
export const userId = state => state.auth.userId

export default authSlice.reducer